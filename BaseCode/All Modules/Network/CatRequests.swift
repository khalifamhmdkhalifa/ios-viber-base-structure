//
//  CatRequester.swift
//  BaseCode
//
//  Created by khalifa on 7/24/20.
//  Copyright © 2020 khalifa. All rights reserved.
//

import Foundation

enum CatsImagesAPI {
    case search(key: String?, page: Int, limit: Int?)
    private enum ParametersKeys: String {
        case page, limit
    }
    var endPoint: String {
        switch self {
        case .search(_, _, _):
            return "images/search"
        }
    }
    var parameters: [String: AnyObject]? {
        switch self {
        case .search(_, let page, let limit):
            return [ParametersKeys.page.rawValue: String(page) as AnyObject, ParametersKeys.limit.rawValue: String(limit ?? 0) as AnyObject]
        }
    }
    
    var method: RequestMethod {
        switch self {
        case .search(_, _, _):
            return .GET
        }
    }
}

extension CatsImagesAPI: RequestProtocol {
    func requestSpecs<T>() -> RequestSpecs<T> where T : Decodable, T : Encodable {
        return RequestSpecs(method: method, URLString: endPoint, parameters: parameters)
    }
}

protocol RequestProtocol {
    func requestSpecs<T: Codable>() -> RequestSpecs<T>
}

