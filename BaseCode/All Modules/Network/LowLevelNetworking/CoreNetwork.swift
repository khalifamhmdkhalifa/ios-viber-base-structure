//
//  CoreNetwork.swift
//  ios-task
//
//  Created by Khalifa on 10/3/18.
//  Copyright © 2018 khalifa. All rights reserved.
//

import UIKit

class NetworkModule {
    var corNetworkInstance: CoreNetworkProtocol = CoreNetwork()
}

class CoreNetwork {
    static var sharedInstance: CoreNetworkProtocol = CoreNetwork()
    
    fileprivate lazy var networkCommunication: NetworkingInterface = {
        AlamofireAdaptor(baseURL: HostService.getBaseURL(), headers: HostService.headers)
    }()
}

extension CoreNetwork: CoreNetworkProtocol {
    func makeRequest<T: Codable>(request: RequestSpecs<T>, completion: @escaping (T?, Error?) -> Void){
        networkCommunication.request(request, completionBlock: completion)
    }
}

protocol CoreNetworkProtocol {
    func makeRequest<T: Codable>(request: RequestSpecs<T>, completion: @escaping (T?, Error?) -> Void)
}
