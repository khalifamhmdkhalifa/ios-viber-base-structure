//
//  APIServiceSettings.swift
//  ios-task
//
//  Created by Khalifa on 10/3/18.
//  Copyright © 2018 khalifa. All rights reserved.
//

import UIKit

struct HostService {
    static func getBaseURL() -> String {
        return "https://api.thecatapi.com/v1/"
    }
    static var headers: [String: String] {
        get {
            return HostService.apiHeaders()}
    }
    
    fileprivate static func apiHeaders() -> [String: String] {
        var headers:Dictionary<String,String> = Dictionary<String,String>()
        headers["x-api-key"] = "17d0b4ca-ea80-4d37-9a8e-07d05a4a0c5f"
    
        return headers
    }
    
}
