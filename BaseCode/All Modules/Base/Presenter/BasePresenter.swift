//
//  BasePresenter.swift
//  BaseCode
//
//  Created by khalifa on 4/21/20.
//  Copyright © 2020 khalifa. All rights reserved.
//

import Foundation

class BasePresenter<View, Interactor, Wireframe> {
    @WeakReference
    var view: View?
    let interactor: Interactor?
    let wireframe: Wireframe?
    
    init(view: View, interactor: Interactor, wireframe: Wireframe?) {
        self.view = view
        self.interactor = interactor
        self.wireframe = wireframe
    }
    
    func viewDidLoad() {
        
    }
    
    func viewWillAppear() {
        
    }
    
    func viewDidAppear() {
        
    }
    
    func viewWillDisappear() {
        
    }
    
    func viewDidDisappear() {
        
    }
}

extension BasePresenter: BasePresenterProtocol {
}
