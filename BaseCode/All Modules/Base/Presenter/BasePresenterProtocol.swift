//
//  BasePresenter.swift
//  BaseCode
//
//  Created by khalifa on 4/21/20.
//  Copyright © 2020 khalifa. All rights reserved.
//

import Foundation

protocol BasePresenterProtocol {
    func viewDidLoad()
    func viewWillAppear()
    func viewDidAppear()
    func viewWillDisappear()
    func viewDidDisappear()
}

@propertyWrapper
struct WeakReference<T> {
    private weak var value: AnyObject?
    var wrappedValue: T? {
        get { return value as? T }
        set { value = newValue as AnyObject }
    }
    
    init(wrappedValue: T?) {
        self.wrappedValue = wrappedValue
    }
}
