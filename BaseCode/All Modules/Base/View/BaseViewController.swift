//
//  BaseViewController.swift
//  BaseCode
//
//  Created by khalifa on 4/21/20.
//  Copyright © 2020 khalifa. All rights reserved.
//

import UIKit

class BaseViewController<Presenter>: UIViewController {
    var presenter: Presenter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        (presenter as? BasePresenterProtocol)?.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        (presenter as? BasePresenterProtocol)?.viewWillAppear()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        (presenter as? BasePresenterProtocol)?.viewDidAppear()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        (presenter as? BasePresenterProtocol)?.viewWillDisappear()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        (presenter as? BasePresenterProtocol)?.viewDidDisappear()
    }
}
/*
extension BaseViewController: BaseView where Presenter: BasePresenterProtocol {
    typealias Presenter = Presenter
}*/

