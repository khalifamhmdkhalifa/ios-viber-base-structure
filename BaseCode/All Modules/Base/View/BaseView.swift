//
//  BaseViewController.swift
//  BaseCode
//
//  Created by khalifa on 4/21/20.
//  Copyright © 2020 khalifa. All rights reserved.
//

import UIKit


protocol BaseView: class {
    associatedtype Presenter
    var presenter: Presenter? { get set }
}

protocol LoadingViewContainer {
    func showLoading()
    func hideloading()
}
