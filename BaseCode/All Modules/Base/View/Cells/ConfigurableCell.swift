//
//  ConfigurableCell.swift
//  BaseCode
//
//  Created by khalifa on 4/21/20.
//  Copyright © 2020 khalifa. All rights reserved.
//

import UIKit

protocol ConfigurableCell: class {
    associatedtype CellModel
    func configure(_ model: CellModel)
}

protocol ConfigurableInteractiveCell {
    associatedtype DelegateType
    associatedtype CellModel
    func configure(_ model: CellModel, delegate: DelegateType?, indexPath: IndexPath)
    var delegate: DelegateType? {get set}
    var indexPath: IndexPath? {get set}
}

protocol CollectionContainer {
    associatedtype CellType
    associatedtype CellModel
    var items: [CellModel]? {get}
    func configureCell(cell: CellType, model: CellModel, indexPath: IndexPath)
}

extension CollectionContainer where CellType: ConfigurableCell, CellType.CellModel == CellModel {
    func configureCell(cell: CellType, model: CellModel, indexPath: IndexPath) {
        cell.configure(model)
    }
}

extension CollectionContainer where CellType: ConfigurableInteractiveCell, CellType.CellModel == CellModel {
    func configureCell(cell: CellType, model: CellModel, indexPath: IndexPath) {
        cell.configure(model, delegate: (self as? CellType.DelegateType), indexPath: indexPath)
    }
}

extension CollectionContainer where CellType: UICollectionViewCell {
    func getConfiguredCell(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let item = items?[indexPath.item], let cell: CellType = collectionView.dequeueReusableCell(indexPath: indexPath) else { return UICollectionViewCell.init() }
        configureCell(cell: cell, model: item, indexPath: indexPath)
        return cell
    }
    
    func registerCollectionViews(_ collection: UICollectionView) {
        collection.registerCellType(CellType.self)
    }
}
