//
//  UITableViewCellExtensions.swift
//  BaseCode
//
//  Created by khalifa on 7/29/20.
//  Copyright © 2020 khalifa. All rights reserved.
//

import UIKit


extension UITableViewCell {
    class var identifier: String { return "\(self)" }
    class var nibFileName: String { return "\(self)" }
}

