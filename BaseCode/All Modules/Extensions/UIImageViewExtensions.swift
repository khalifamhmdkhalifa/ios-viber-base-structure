//
//  UIImageViewExtensions.swift
//  BaseCode
//
//  Created by khalifa on 7/29/20.
//  Copyright © 2020 khalifa. All rights reserved.
//

import Foundation
import SDWebImage

extension UIImageView {
    
    func loadImage(_ url: URL, withLoadingIndicator: Bool = false) {
        if withLoadingIndicator {
            sd_imageIndicator = SDWebImageActivityIndicator.gray
        }
        sd_setImage(with: url)
    }
    
    func loadImage(_ stringUrl: String?, withLoadingIndicator: Bool = false) {
        guard let stringUrl = stringUrl, let url = URL(string: stringUrl) else { return }
        loadImage(url, withLoadingIndicator: withLoadingIndicator)
    }
    
    func stopImageloading() {
        sd_cancelCurrentImageLoad()
        sd_cancelCurrentImageLoad()
    }
}
