//
//  UITableViewDataSourceExtensions.swift
//  BaseCode
//
//  Created by khalifa on 7/29/20.
//  Copyright © 2020 khalifa. All rights reserved.
//

import UIKit

extension UITableView {
    func registerCellType<T: UITableViewCell>(_ type: T.Type) {
        self.register(UINib.init(nibName: type.nibFileName, bundle: nil), forCellReuseIdentifier: type.identifier)
    }
    
    func dequeueReusableCell<T: UITableViewCell>(indexPath: IndexPath? = nil) -> T? {
        if let indexPath = indexPath {
            return dequeueReusableCell(withIdentifier: T.identifier, for: indexPath) as? T
        } else {
            return dequeueReusableCell(withIdentifier: T.identifier) as? T
        }
    }
}
