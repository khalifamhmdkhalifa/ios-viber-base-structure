//
//  ArrayExtensions.swift
//  BaseCode
//
//  Created by khalifa on 7/24/20.
//  Copyright © 2020 khalifa. All rights reserved.
//

import Foundation

extension Array {
    
    subscript(safe index: Int) -> Element? {
        get {
            guard index < count else { return nil }
            return self[index]
        }
    }
}
