//
//  UICollectionViewCellExtensions.swift
//  Mumzworld
//
//  Created by Khalifa on 4/14/19.
//  Copyright © 2019 Softxpert. All rights reserved.
//

import UIKit

extension UICollectionViewCell {
    class var identifier: String { return "\(self)" }
    class var nibFileName: String { return "\(self)" }
}
