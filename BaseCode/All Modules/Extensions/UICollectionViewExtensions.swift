//
//  UICollectionViewExtensions.swift
//  BaseCode
//
//  Created by khalifa on 7/29/20.
//  Copyright © 2020 khalifa. All rights reserved.
//

import UIKit

extension UICollectionView {
    func registerCellType<T: UICollectionViewCell>(_ type: T.Type) {
        self.register(UINib(nibName: type.nibFileName, bundle: nil), forCellWithReuseIdentifier: type.identifier)
    }
    
    func dequeueReusableCell<T: UICollectionViewCell>(indexPath: IndexPath) -> T? {
        return dequeueReusableCell(withReuseIdentifier: T.identifier, for: indexPath) as? T
    }
}
