//
//  UIViewControllerExtensions.swift
//  BaseCode
//
//  Created by khalifa on 7/29/20.
//  Copyright © 2020 khalifa. All rights reserved.
//

import UIKit

extension UIViewController {
    class var identifier: String {
        return "\(self)"
    }
    class var storyboardName: String {
        return "\(self)"
    }
    
    class func createFromStoryboard() -> Self? {
        let mainStoryboardIpad = UIStoryboard(name: storyboardName, bundle: nil)
        guard let controller = mainStoryboardIpad.instantiateViewController(withIdentifier: identifier) as? Self else { return nil }
        return controller
    }
    
}
