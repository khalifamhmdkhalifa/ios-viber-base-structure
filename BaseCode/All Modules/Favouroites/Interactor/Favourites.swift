//
//  Favourites.swift
//  BaseCode
//
//  Created by khalifa on 7/24/20.
//  Copyright © 2020 khalifa. All rights reserved.
//

import Foundation


class Favourites {
    private static let favouritesKey = "Favourites"
    public static var shared = Favourites()
    private var storedFavourites: [CatModel]? {
        if let json = UserDefaults.standard.object(forKey: Favourites.favouritesKey) as? Data {
            let decoder = JSONDecoder()
            if let loadedData = try? decoder.decode([CatModel].self, from: json) {
                return loadedData
            } else {
                return nil
            }
        }
        return nil
    }
    private lazy var favouritesSet: Set<String> = Set(storedFavourites?.compactMap({$0.id}) ?? [])
    
    func isInFavourites(_ model: CatModel) -> Bool {
        if let id = model.id {
            return favouritesSet.contains(id)
        }
        return false
    }
    
    private init() {
    }
    
    private func didUpdateFavourites(updatedFavourites: [CatModel]) {
        favouritesSet = Set(storedFavourites?.compactMap({$0.id}) ?? [])
    }
    
    func loadFavourites(completion: @escaping ([CatModel]?)-> Void) {
        DispatchQueue.global(qos: .utility).async {
            let favourites = self.storedFavourites
            DispatchQueue.main.async {
                completion(favourites)
            }
        }
    }
    
    func addToFavourites(model: CatModel, completion: @escaping (Bool)->Void) {
        DispatchQueue.global(qos: .utility).async {
            var favourites = self.storedFavourites ?? []
            guard favourites.filter ({ $0.id == model.id }).isEmpty else {
                DispatchQueue.main.async { completion(true) }
                return
            }
            favourites.append(model)
            self.storeFavourites(favourites)
            self.didUpdateFavourites(updatedFavourites: favourites)
            DispatchQueue.main.async { completion(true) }
        }
    }
    
    private func storeFavourites(_ favourites: [CatModel]) {
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(favourites) {
            let defaults = UserDefaults.standard
            defaults.set(encoded, forKey: Favourites.favouritesKey)
        }
    }
    
    func removeFromFavourites(model: CatModel, completion: @escaping (Bool)->Void) {
        DispatchQueue.global(qos: .utility).async {
            var favourites = self.storedFavourites ?? []
            guard let index = favourites.firstIndex (where:{ $0.id == model.id }) else {
                DispatchQueue.main.async { completion(false) }
                return
            }
            favourites.remove(at: index)
            self.storeFavourites(favourites)
            self.didUpdateFavourites(updatedFavourites: favourites)
            DispatchQueue.main.async { completion(true) }
        }
    }
}

extension Favourites: FavouritesInteractor {
    
}

protocol FavouritesInteractor {
    func loadFavourites(completion: @escaping ([CatModel]?)-> Void)
}
