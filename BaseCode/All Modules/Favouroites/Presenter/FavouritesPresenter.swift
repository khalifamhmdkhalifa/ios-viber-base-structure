//
//  FavouritesPresenter.swift
//  BaseCode
//
//  Created by khalifa on 7/24/20.
//  Copyright © 2020 khalifa. All rights reserved.
//

import Foundation

protocol FavouriesPresenterProtocol: BasePresenterProtocol {
}

class FavouriesPresenter: BasePresenter<FavouritesView, FavouritesInteractor, FavouritesWireframe> {
    override func viewDidLoad() {
        interactor?.loadFavourites(completion: { result in
            guard let result = result, !result.isEmpty else {
                self.view?.showMessage("Failed To Load Favourites")
                return
            }
            self.view?.show(result)
        })
    }
}

extension FavouriesPresenter: FavouriesPresenterProtocol {
}
