//
//  FavouriteViewController.swift
//  BaseCode
//
//  Created by khalifa on 7/24/20.
//  Copyright © 2020 khalifa. All rights reserved.
//

import UIKit

class FavouriteViewController: BaseViewController<FavouriesPresenterProtocol> {
    @IBOutlet weak var collection: UICollectionView! {
        didSet {
            self.registerCollectionViews(collection)
        }
    }
    var items: [CatModel]? = []
}

extension FavouriteViewController: CollectionContainer {
    typealias CellType = FavouriteCell
    typealias CellModel = CatModel
}

extension FavouriteViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = Int((collectionView.frame.width - (collectionView.contentInset.left +  collectionView.contentInset.right))/3)
        return CGSize(width: width, height: Int(collectionView.frame.height)/5);
    }
}

extension FavouriteViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return getConfiguredCell(collectionView, cellForItemAt: indexPath)
    }
}

extension FavouriteViewController: FavouritesView {
    func showMessage(_ message: String) {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func show(_ items: [CatModel]) {
        self.items = items
        collection.reloadData()
    }
}

protocol FavouritesView: BaseView {
    func show(_ items:[CatModel])
    func showMessage(_ message: String)
}
