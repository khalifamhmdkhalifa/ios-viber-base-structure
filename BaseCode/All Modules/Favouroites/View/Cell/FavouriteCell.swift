//
//  FavouritesCell.swift
//  BaseCode
//
//  Created by khalifa on 7/24/20.
//  Copyright © 2020 khalifa. All rights reserved.
//

import SDWebImage
import UIKit

class FavouriteCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    override func prepareForReuse() {
        super.prepareForReuse()
        imageView.stopImageloading()
    }
}

extension FavouriteCell: ConfigurableCell {
    typealias CellModel = CatModel
    
    func configure(_ model: CatModel) {
        self.imageView.loadImage(model.url, withLoadingIndicator: true)
    }
}
