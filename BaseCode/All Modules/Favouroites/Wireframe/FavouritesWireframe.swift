//
//  FavouritesWireframe.swift
//  BaseCode
//
//  Created by khalifa on 7/29/20.
//  Copyright © 2020 khalifa. All rights reserved.
//

import UIKit

class FavouritesWireframe {
    private weak var parentNavigationController: UINavigationController?
    
    func showFavouritesScreen(in parentNavigationController: UINavigationController? = nil) {
        guard let parentNavigationController = parentNavigationController ?? self.parentNavigationController, let controller = FavouritesModule.createAssembledFavouritesViewController() else { return }
        self.parentNavigationController = parentNavigationController
        parentNavigationController.pushViewController(controller, animated: false)
    }
}
