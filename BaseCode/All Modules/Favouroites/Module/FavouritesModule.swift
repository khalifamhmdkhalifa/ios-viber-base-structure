//
//  FavouritesModule.swift
//  BaseCode
//
//  Created by khalifa on 7/29/20.
//  Copyright © 2020 khalifa. All rights reserved.
//

import Foundation

class FavouritesModule {
    static func createAssembledFavouritesViewController(wireframe: FavouritesWireframe? = nil )-> FavouriteViewController? {
        guard let viewController = FavouriteViewController.createFromStoryboard() else { return nil }
        let interactor = Favourites.shared
        let presenter = FavouriesPresenter(view: viewController, interactor: interactor, wireframe: wireframe ?? FavouritesWireframe())
        viewController.presenter = presenter
        return viewController
    }
}
