//
//  HomeModule.swift
//  BaseCode
//
//  Created by khalifa on 7/29/20.
//  Copyright © 2020 khalifa. All rights reserved.
//

import Foundation

class HomeModule  {
    static func getAssembledHomeVeiwController(wireFrame: HomeWireFrame? = nil) -> HomeViewController? {
        guard let viewController = HomeViewController.createFromStoryboard() else { return nil }
        let interactor = HomeInteractor(coreNetwork: CoreNetwork.sharedInstance)
        let presenter = HomePresenter(view: viewController, interactor: interactor, wireframe: wireFrame ?? HomeWireFrame())
        viewController.presenter = presenter
        return viewController
    }
}
