//
//  HomeWirframe.swift
//  BaseCode
//
//  Created by khalifa on 7/29/20.
//  Copyright © 2020 khalifa. All rights reserved.
//

import UIKit

protocol HomeWireFrameProtocol {
    func showFavourites()
}

class HomeWireFrame: HomeWireFrameProtocol {
    weak var parentNavigationController: UINavigationController?
    
    func showFavourites() {
        FavouritesWireframe().showFavouritesScreen(in: parentNavigationController)
    }
}
