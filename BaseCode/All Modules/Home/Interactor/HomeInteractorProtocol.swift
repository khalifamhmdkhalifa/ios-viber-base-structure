//
//  HomeInteractorProtocol.swift
//  BaseCode
//
//  Created by khalifa on 4/21/20.
//  Copyright © 2020 khalifa. All rights reserved.
//

import Foundation

protocol HomeInteractorProtocol: class {
    func loadCats(page: Int, completion: @escaping ([CatModel]?, Error?)->Void)
    func addToFavourites(model: CatModel, completion: @escaping (Bool)-> Void)
    func removeFromFavourites(model: CatModel, completion: @escaping (Bool)-> Void)
}

class HomeInteractor: HomeInteractorProtocol {
    var coreNetwork: CoreNetworkProtocol
    
    init(coreNetwork: CoreNetworkProtocol) {
        self.coreNetwork = coreNetwork
    }
    
    func removeFromFavourites(model: CatModel, completion: @escaping (Bool) -> Void) {
        Favourites.shared.removeFromFavourites(model: model, completion: completion)
    }
    
    func addToFavourites(model: CatModel, completion: @escaping (Bool)-> Void) {
        Favourites.shared.addToFavourites(model: model, completion: completion)
    }
    
    func loadCats(page: Int, completion: @escaping ([CatModel]?, Error?)-> Void) {
        CoreNetwork.sharedInstance.makeRequest(request: CatsImagesAPI.search(key: nil, page: page, limit: nil).requestSpecs(), completion: completion)
    }
    
}
