//
//  HomePresenter.swift
//  BaseCode
//
//  Created by khalifa on 4/21/20.
//  Copyright © 2020 khalifa. All rights reserved.
//

import Foundation

protocol HomePresenterProtocol: BasePresenterProtocol {
    func didScrollToLastItem()
    func didClickShowFavourites()
    func didClickAddToFavourites(index: Int)
}

class HomePresenter: BasePresenter<HomeViewProtocol, HomeInteractor, HomeWireFrame> {
    private var currenyPage = 0
    private var catsModels: [CatModel] = []
    private var isLoading = false
    
    override func viewDidLoad() {
        guard !isLoading else { return }
        view?.showLoading()
        loadCats(page: 1) {
            self.view?.hideLoading()
        }
    }
    
    private func loadCats(page: Int, completion: (()->Void)? = nil) {
        isLoading = true
        interactor?.loadCats(page: page) { [weak self] (result, error) in
            completion?()
            self?.isLoading = false
            if let _ = error {
                self?.view?.showMessage("Couldn't load page \(page)")
            } else if let result = result {
                self?.didLoadCatsPage(models: result, page: page)
            }
        }
    }
    
    func updateIsInFavourites() {
        for index in 0..<catsModels.count {
            catsModels[index].isInFavourites = Favourites.shared.isInFavourites(catsModels[index])
        }
    }
    
    func didLoadCatsPage(models: [CatModel], page: Int) {
        if page == 1 {
            self.catsModels = models
        } else {
            self.catsModels.append(contentsOf: models)
        }
        currenyPage = max(page, currenyPage)
        updateIsInFavourites()
        view?.show(catsModels)
    }
    
    func addToFavourites(_ model: CatModel ){
        self.interactor?.addToFavourites(model: model) { [weak self] added in
            guard let self = self else { return }
            if added {
                self.view?.showMessage("Added to Favourites")
            } else {
                self.view?.showMessage("Faild to add to Favourites")
            }
            self.updateIsInFavourites()
            self.view?.show(self.catsModels)
        }
    }
    
    func removeFromFavourites(_ model: CatModel) {
        self.interactor?.removeFromFavourites(model: model) { [weak self] added in
            guard let self = self else { return }
            if added {
                self.view?.showMessage("Removed from Favourites")
            } else {
                self.view?.showMessage("Faild to remove from Favourites")
            }
            self.updateIsInFavourites()
            self.view?.show(self.catsModels)
        }
    }
}

extension HomePresenter: HomePresenterProtocol {
    func didClickShowFavourites() {
        wireframe?.showFavourites()
    }
    
    func didClickAddToFavourites(index: Int) {
        guard let model = catsModels[safe: index] else { return }
        if model.isInFavourites == true {
            removeFromFavourites(model)
        } else {
            addToFavourites(model)
        }
    }
    
    func didScrollToLastItem() {
        guard !isLoading else { return }
        loadCats(page: currenyPage + 1)
    }
}

extension HomePresenterProtocol {
    typealias View = HomeViewProtocol
    typealias Interactor = HomeViewProtocol
}



