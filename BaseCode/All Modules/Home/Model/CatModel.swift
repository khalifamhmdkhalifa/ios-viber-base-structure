//
//  CatModel.swift
//  BaseCode
//
//  Created by khalifa on 7/24/20.
//  Copyright © 2020 khalifa. All rights reserved.
//

import Foundation

struct CatModel: Codable {
    var url: String?
    var id: String?
    var isInFavourites: Bool? = false
}
