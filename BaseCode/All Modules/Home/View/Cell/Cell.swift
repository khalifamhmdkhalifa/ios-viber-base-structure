//
//  Cell.swift
//  BaseCode
//
//  Created by khalifa on 4/21/20.
//  Copyright © 2020 khalifa. All rights reserved.
//

import UIKit
import SDWebImage

class Cell: UICollectionViewCell {
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var addToFavouritesButton: UIButton!
    var indexPath: IndexPath?
    var delegate: CellDelegate?
    
    @IBAction func didClickAddToFavourites(_ sender: Any) {
        guard let delegate = delegate, let index = indexPath?.row else {
            return
        }
        delegate.didClickAddToFavoutrites(index: index)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        image.stopImageloading()
        addToFavouritesButton.setTitle("",for: .normal)
    }
}

extension Cell: ConfigurableInteractiveCell {
    typealias CellModel = CatModel
    
    func configure(_ model: CatModel, delegate: CellDelegate?, indexPath: IndexPath) {
        self.delegate = delegate
        guard let urlString = model.url, let url = URL(string: urlString) else { return }
        image.sd_imageIndicator = SDWebImageActivityIndicator.gray
        image.sd_setImage(with: url)
        self.indexPath = indexPath
        addToFavouritesButton.setTitle(model.isInFavourites == true ? "Remove from Favourites" : "Add to Favourites" ,for: .normal)
    }
}

protocol CellDelegate {
    func didClickAddToFavoutrites(index: Int)
}
