//
//  HomeViewController.swift
//  BaseCode
//
//  Created by khalifa on 4/21/20.
//  Copyright © 2020 khalifa. All rights reserved.
//

import UIKit

class HomeViewController: BaseViewController<HomePresenterProtocol> {
    @IBOutlet weak var collection: UICollectionView! {
        didSet {
            self.registerCollectionViews(collection)
        }
    }
    
    @IBOutlet weak var loadingView: UIActivityIndicatorView! {
        didSet {
            loadingView.hidesWhenStopped = true
            loadingView.stopAnimating()
        }
    }
    var items: [CatModel]?
    
    @IBAction func didClickShowFavourites() {
        presenter?.didClickShowFavourites()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Home"
    }
}

extension HomeViewController: CollectionContainer {
    typealias CellType = Cell
    typealias CellModel = CatModel
}

extension HomeViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = Int((collectionView.frame.width - (collectionView.contentInset.left +  collectionView.contentInset.right))/3)
        return CGSize(width: width, height: Int(collectionView.frame.height)/5);
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let count = items?.count, indexPath.row == count - 1 {
            presenter?.didScrollToLastItem()
        }
    }
}

extension HomeViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return getConfiguredCell(collectionView, cellForItemAt: indexPath)
    }
    
}

extension HomeViewController: HomeViewProtocol {
    func showMessage(_ message: String) {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showLoading() {
        loadingView.isHidden = false
        loadingView.startAnimating()
    }
    
    func hideLoading() {
        loadingView.stopAnimating()
        loadingView.isHidden = true
    }
    
    func show(_ items: [CatModel]) {
        self.items = items
        collection.reloadData()
    }
}

extension HomeViewController: CellDelegate {
    func didClickAddToFavoutrites(index: Int) {
        self.presenter?.didClickAddToFavourites(index: index)
    }
}

protocol HomeViewProtocol: BaseView {
    func show(_ items: [CatModel])
    func showLoading()
    func hideLoading()
    func showMessage(_ message: String)
}

