//
//  SceneDelegate.swift
//  BaseCode
//
//  Created by khalifa on 4/21/20.
//  Copyright © 2020 khalifa. All rights reserved.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    var window: UIWindow?
    
    @available(iOS 13.0, *)
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        let wireframe = HomeWireFrame()
        guard let homeViewController = HomeModule.getAssembledHomeVeiwController(wireFrame: wireframe) else { return }
        let navigationController = UINavigationController(rootViewController: homeViewController)
        wireframe.parentNavigationController = navigationController
        self.window?.rootViewController = navigationController
        self.window?.makeKeyAndVisible()
        guard let _ = (scene as? UIWindowScene) else { return }
    }
}

